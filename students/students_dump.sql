CREATE DATABASE  IF NOT EXISTS `students` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `students`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: students
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `marks` (
  `marks_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `subject` varchar(45) DEFAULT NULL COMMENT 'предмет',
  `mark` int(11) DEFAULT NULL COMMENT 'оценка',
  `student_id` int(11) NOT NULL COMMENT 'id студента',
  PRIMARY KEY (`marks_id`),
  KEY `student_id_idx` (`student_id`),
  CONSTRAINT `student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='оценки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks`
--

LOCK TABLES `marks` WRITE;
/*!40000 ALTER TABLE `marks` DISABLE KEYS */;
INSERT INTO `marks` VALUES (1,'Java',5,1),(2,'C++',3,1),(3,'Java',3,2),(4,'C++',4,2),(5,'Java',5,3),(6,'C++',5,3),(7,'Java',2,4),(8,'C++',2,4),(9,'Java',3,5),(10,'C++',4,5),(11,'Java',4,6),(12,'C++',4,6),(13,'C#',5,6),(14,'Java',5,7),(15,'C++',4,7),(16,'C#',4,7),(17,'Java',3,8),(18,'C++',3,8),(19,'Java',2,9),(20,'C++',2,9),(21,'C#',2,9),(22,'Java',2,10),(23,'C++',3,10),(24,'Java',4,11),(25,'C++',3,11),(26,'Java',4,12),(27,'C++',5,12),(28,'C#',5,12),(29,'Java',5,13),(30,'C++',5,13),(31,'C#',5,13),(32,'Java',4,14),(33,'C++',4,14),(34,'C#',4,14),(35,'Java',2,15),(36,'C++',2,15),(37,'Java',3,16),(38,'C++',3,16),(39,'Java',4,17),(40,'C++',4,17),(41,'Java',5,18),(42,'C++',4,18),(43,'Java',5,19),(44,'C++',5,19),(45,'Java',4,20),(46,'C++',5,20);
/*!40000 ALTER TABLE `marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(45) DEFAULT NULL COMMENT 'имя',
  `group` varchar(45) DEFAULT NULL COMMENT 'группа',
  `sex` varchar(1) DEFAULT NULL COMMENT 'пол',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Некрасов Тимофей','ИСТ-611','м'),(2,'Родионов Тимур','ИСТ-611','м'),(3,'Соболев Иван','ИСТ-611','м'),(4,'Шарова Римма','ИКВТ-74','ж'),(5,'Наумов Денис','ИСТ-611','м'),(6,'Власов Павел','ИКПИ-71','м'),(7,'Жданов Руслан','ИКПИ-71','м'),(8,'Сергеевна Нина','ИКВТ-74','ж'),(9,'Блозин Евгений','ИКПИ-71','м'),(10,'Куликов Игорь','ИКБ-51','м'),(11,'Евдокимов Денис','ИКБ-51','м'),(12,'Силин Валентин','ИКПИ-71','м'),(13,'Марков Дмитрий','ИКПИ-71','м'),(14,'Панова Анастасия','ИКПИ-71','ж'),(15,'Кулагин Кирилл','ИСТ-611','м'),(16,'Лобанова Надежда','ИКБ-51','ж'),(17,'Силин Дмитрий','ИКБ-51','м'),(18,'Анисимов Игорь','ИКБ-51','м'),(19,'Фокина Елена','ИКБ-51','ж'),(20,'Зуев Вадим','ИКВТ-74','м');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-01 20:10:16
