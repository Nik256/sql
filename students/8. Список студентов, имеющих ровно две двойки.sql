SELECT name FROM student s JOIN marks m 
ON s.student_id = m.student_id 
WHERE mark = 2
GROUP BY name
HAVING COUNT(mark) = 2