DELETE FROM marks
WHERE student_id IN (
SELECT * FROM
(SELECT student_id
FROM marks 
WHERE mark = 2
GROUP BY student_id
HAVING COUNT(*) = 3) temp);

DELETE FROM student WHERE student_id NOT IN (
SELECT student_id FROM marks);