CREATE DATABASE  IF NOT EXISTS `online_auction` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `online_auction`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: online_auction
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bids` (
  `bid_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id ставки',
  `bid_date` date DEFAULT NULL COMMENT 'дата ставки',
  `bid_value` double DEFAULT NULL COMMENT 'размер ставки',
  `items_item_id` int(11) NOT NULL COMMENT 'id лота',
  `users_user_id` int(11) NOT NULL COMMENT 'id пользователя',
  PRIMARY KEY (`bid_id`),
  KEY `user_id_idx` (`users_user_id`),
  KEY `item_id` (`items_item_id`),
  CONSTRAINT `item_id` FOREIGN KEY (`items_item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ставки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
INSERT INTO `bids` VALUES (1,'2019-01-01',1000000,1,2),(2,'2019-01-01',230000,2,5),(3,'2019-01-01',95000,5,5),(4,'2019-01-01',85000,6,5),(5,'2019-01-02',85000,7,4),(6,'2019-01-02',58000,8,5),(7,'2019-01-02',240000,2,3),(8,'2019-01-02',60000,9,2),(9,'2019-01-02',40000,10,1),(10,'2019-01-03',1100000,1,1),(11,'2019-01-03',250000,2,4),(12,'2019-01-03',87000,7,3),(13,'2019-01-03',200000,3,1),(14,'2019-01-03',100000,5,3),(15,'2019-01-03',160000,4,2);
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id лота',
  `title` varchar(100) DEFAULT NULL COMMENT 'название',
  `description` varchar(255) DEFAULT NULL COMMENT 'описание',
  `start_price` double DEFAULT NULL COMMENT 'стартовая цена',
  `bid_increment` double DEFAULT NULL COMMENT 'инкремент ставки',
  `start_date` date DEFAULT NULL COMMENT 'дата начала',
  `stop_date` date DEFAULT NULL COMMENT 'дата конца',
  `by_it_now` binary(1) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL COMMENT 'id пользователя',
  PRIMARY KEY (`item_id`),
  KEY `user_id_idx` (`users_user_id`),
  CONSTRAINT `user_id0` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='лоты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Немецкая швейная машинка Zinger 1908 г.','Можно вывозить за границу, вековой товар 110 лет, бережно хранилась, есть все необходимые документы, машинка полностью рабочем отличном состоянии с коробом.',1000000,50000,'2018-05-12','2019-01-05',NULL,3),(2,'Старинный потир + дискос серебро Франция','Франция, 1930-е годы. Стиль арт Деко. Высота потира : 17, 5 см. Диаметр чаши 12 см. Дискос диаметром 15 см. Общий вес потира и дискоса 820 гр.',230000,10000,'2018-11-05','2019-10-20',NULL,1),(3,'Большая картина \"Встреча охотников\", 1850 г','Картина, акварель, 1850 г., художник Circle JAMES WILLIAM GILES Британская школа, размер 46 * 63 см, рама 69 * 89 см.',200000,8000,'2018-05-12','2025-11-06',NULL,3),(4,'Старинные напольные часы, 19 век','Рабочие,механические,высота часов 187 см ,сделаны в Германии',160000,12000,'2018-05-12','2022-10-23',NULL,4),(5,'Пистоль кремневый, офицерский. Бельгия.','Бельгийский армейский пистолет с кремневым замком, изготовленный в первой половине XIX века. Замок исправен, представленный экземпляр использовался на флоте. Пятка рукояти, скоба и ложевое кольцо изготовлены из латуни.',95000,5000,'2018-07-10','2025-11-06',NULL,1),(6,'Две каминные вазы Ампир XIX Россия','Две каминные вазы Ампир XIX века, в отличном состоянии без сколов и трещин.',85000,3000,'2018-05-12','2025-08-25',NULL,1),(7,'Кувшин под вино и бокалы янтарного стекла начала 20 века.','Кувшин под вино начала 20 века. ',85000,2000,'2018-07-10','2019-10-20',NULL,5),(8,'Картина \"Дорога в горах\". Лагорио. Копия. 1953 г.','Холст, масло, багет. Размер 96см х 69см.',58000,1000,'2018-11-05','2025-11-06',NULL,2),(9,'Зеркало резное ( старинное)','Зеркало изготовлено в 1903 году. реставрация 2018 г, ручная резьба .',60000,1500,'2018-01-15','2025-08-25',NULL,5),(10,'Старинная трость японского воина Сикомидзуэ с рукоятью из кости буйвола \"Самурай\".','Рукоять - кость буйвола, украшенная изысканной резьбой. Стик - бамбук. Наконечник – металл.',40000,1000,'2018-11-05','2019-10-20',NULL,2);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id пользователя',
  `full_name` varchar(45) DEFAULT NULL COMMENT 'полное имя',
  `billing_address` varchar(100) DEFAULT NULL COMMENT 'платёжный адрес',
  `login` varchar(20) DEFAULT NULL COMMENT 'логин',
  `password` varchar(45) DEFAULT NULL COMMENT 'пароль',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='пользователи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Покровский Януарий Петрович','г. Харабали, ул. Живописная, дом 10, квартира 198','pokrov223','1q2w3e4R5t'),(2,'Дмитриева Лиана Ивановна ','г. Москва, ул. Выползов Переулок, дом 21, квартира 104','dlian3','aFhkl1g44f'),(3,'Леонтьев Степан Романович','г. Фаленки, ул. Дивизионная, дом 62, квартира 32','sleon42','zfb4hGn35f'),(4,'Осипов Никита Георгиевич','г. Венев, ул. Беговой Проезд, дом 24, квартира 148','nosipov12','grtGF32f2'),(5,'Петров Авраам Антонович','г. Кошхатау, ул. Матроса Железняка Бульвар, дом 56, квартира 30','apetrov453','jl4Hfs7Idf');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-02 19:12:56
